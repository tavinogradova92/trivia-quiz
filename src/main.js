import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import router from './router';
import Vuex from "vuex";
import axios from 'axios';

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.config.productionTip = false

// storing data that will be used for the results page
const store = new Vuex.Store({
  state: {
      score: 0, // <-- final score 
      currentQuestionIndex: 0, // <-- keeps the current question index to render through the list of questions
      questions: [],
      chosenAnswers: [] // <-- keeps an array of answered chosen by the user
  },
  getters: {
    questions: state => {
      return state.questions
    }
  },
  mutations: {
      increment(state) { // increments score each time the user chooses the right answer
        state.score += 10;
      },
      incrementIndex(state) { // increments index when user answers
        state.currentQuestionIndex ++;
      },
      SET_Questions (state, questions) {
        state.questions = questions
      }
  },
  actions: {
    fetchQuestions({commit}) {
      axios.get('https://opentdb.com/api.php?amount=10&category=27&difficulty=medium')
            .then(res => this.questions = res.data.results)
            .then(questions => {
              commit('SET_Questions', questions)
            })
            .catch(err => console.log(err))
    }
  }
 });

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')


